<!DOCTYPE html>
<html>
<head>
	<title>LSS Vente de tickets</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" href="css/vendors/bootstrap.css">
	<link rel="stylesheet" href="css/vendors/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
<!--	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0.beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3S0dhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
-->
</head>
<body>
	<!--Header-->
	<header>
		<div class="container">
			<div class="row">
				<!--Logo-->
				<div class="col-lg-3">
					<div class="logo">
						<h2><a href="#">Rifou</a></h2>
					</div>
				</div>
				<!--Menu-->
				<nav class="col-lg-9">
					<ul class="nav-list">
						<li class="list"><a href="#">Accueil</a></li>
						<li class="list"><a href="#">Curriculum Vita</a></li>
						<li class="list"><a href="#">Réalisations</a></li>
						<li class="list"><a href="#">Objectif</a></li>
						<li class="list"><a href="#">Forum</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<section>
		<div class="slider-area section">
		    <div id="hero-slider" class="slides">	
		        <img src="riff.jpg" width="100%" alt="" title="#slider-caption1"/>
		    </div>
		</div>
	    <div class="">
	    	<div class="">
	    		<div class="for">
		    	<form name="formulaire" method="post" action="base.php">
		    		<div class="di">
	                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style="color: #ff9900;font-size: 15;">Se connecter</label><br>
	                   <input type="email" style="background-color: #009999;border-radius: 20px;height: 30px;" name="email" placeholder="mail">
                	</div>
		    		<div class="di"><br>
	                   <input type="password" style="background-color: #009999;border-radius: 20px;height: 30px;" name="password" placeholder="password">
                	</div><br>
		    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="valider" value="connexion" class="btn btn-info"><br><br>
		    		<a href="#"><label style="color: #ff9900;font-size: 15;">S'inscrire</label></a> pour plus de visibilité
		    	</form>
		    </div>
		    </div>
	    </div>
	</section>

	<script src="js/vendors/jquery-3.3.1.min.js"></script>
	<script src="js/vendors/bootstrap.js"></script>
	<script src="js/main.js"></script>
</body>
</html>