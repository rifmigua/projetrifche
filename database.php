<!DOCTYPE html>
<html>
<head>
	<title>Liste avec les options Modifier et Supprimer</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>Liste du Table Personne</h1>
		<table class="table table-bordered table-hover table-stripped">
			<tr>
				<th>ID</th>
				<th>Identifiant</th>
				<th>Nom</th>
				<th>Prenom</th>
				<th>Adresse</th>
				<th>Email</th>
				<th>Sexe</th>
				<th>Action</th>
			</tr>
			<?php
				include "connexion.php";
				$query = "SELECT * FROM personne";
				$result = $pdo->query($query);
				$data = $result->fetchALL();
				for ($i =0;
					$i<count($data) 
					;$i++)
				{
					$id=$data [$i] ["id"];
					$identifiant=$data [$i] ["identifiant"];
					$nom=$data [$i] ["nom"];
					$prenom=$data [$i] ["prenom"];
					$adresse=$data [$i] ["adresse"];
					$email=$data [$i] ["email"];
					$sexe=$data [$i] ["sexe"];
					echo "<tr><td>$id</td>
					<td>$identifiant</td>
					<td>$nom</td>
					<td>$prenom</td>
					<td>$adresse</td>
					<td>$email</td>
					<td>$sexe</td>";
					echo "<td>";
					echo "<a href='delete.php?id=$id' onclick='return confirm(\"Etes vous sur de vouloir supprimer ?\");' class='btn btn-danger'>Supprimer</a>";
					echo "<a href='update.php?id=$id' onclick='return confirm(\"Etes vous sur de vouloir modifier ?\");' class='btn btn-warning'>Modifier</a>";
					echo "</tr>";
				}
				
			?>
			
		</table>
	</div>
</body>
</html>