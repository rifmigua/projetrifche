     <?php

        // Connexion à la base de données

            $pdo = null;
            $dsn = 'mysql: host=localhost; dbname=testphp';
            $dbuser = 'root';
            $pw = '';

            try{
              $pdo = new PDO($dsn, $dbuser, $pw);
              $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch (PDOException $e) {
              echo 'Connexion failed: ' . $e->getMessage();
            }
            return $pdo;
        
?>